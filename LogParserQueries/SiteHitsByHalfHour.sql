SELECT 
    QUANTIZE(TO_LOCALTIME(TO_TIMESTAMP(date, time)), 1800) AS FullDate,
	TO_TIME(FullDate) as HalfHour,
	TO_STRING(TO_LOCALTIME(TO_TIMESTAMP(date, time)),'dddd') as DayOfWeek,	
    COUNT(*) AS Hits 
INTO 'C:\Users\AdminBHK\Desktop\logs\output.csv'
FROM 'C:\Users\AdminBHK\Desktop\logs\*'
WHERE TO_LOCALTIME(TO_TIMESTAMP(date, time))>=TIMESTAMP('2017-03-26 00:00:00','yyyy-MM-dd hh:mm:ss')
	AND TO_LOCALTIME(TO_TIMESTAMP(date, time))<TIMESTAMP('2017-04-02 00:00:00', 'yyyy-MM-dd hh:mm:ss') 
Group By FullDate, HalfHour, DayOfWeek
