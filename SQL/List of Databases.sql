SELECT [dtb].[name]
FROM [sys].[databases] AS [dtb]
WHERE(CAST(CASE
               WHEN [dtb].[name] IN('master', 'model', 'msdb', 'tempdb')
               THEN 1
               ELSE [dtb].[is_distributor]
           END AS BIT) = 0)
ORDER BY [dtb].[name];