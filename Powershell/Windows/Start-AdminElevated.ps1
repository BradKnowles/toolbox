﻿#This script will launch a powershell window as a differnt user. primarialy this will be used to launch an administrative 
#powershell prompt as your domain admin account.
$AdminName= Read-Host "Enter your domain admin name starting with quantum\ EG:quantum\admin.name"
Start-Process powershell.exe -Credential $AdminName -NoNewWindow -ArgumentList "Start-Process powershell.exe -Verb runAs"