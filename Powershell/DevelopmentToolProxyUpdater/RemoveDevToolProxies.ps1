
function Find-Git {
    $gitPath = "C:\Program Files\Git\cmd\git.exe"
    $vsGitPath = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\Common7\IDE\CommonExtensions\Microsoft\TeamFoundation\Team Explorer\Git\cmd\git.exe"

    if (Test-Path -Path $gitPath) {
        return $gitPath
    }

    if (Test-Path -Path $vsGitPath) {
        return $vsGitPath
    }

    return ""

}

function Find-Npm {
    $nodePath = "C:\Program Files\nodejs\npm.cmd"
    $vsNodePath = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\Microsoft\VisualStudio\NodeJs\npm.cmd"

    if (Test-Path -Path $nodePath) {
        return $nodePath
    }

    if (Test-Path -Path $vsNodePath) {
        return $vsNodePath
    }

    return ""

}


function Get-NugetFromWeb {    
    $sourceNugetExe = "https://dist.nuget.org/win-x86-commandline/v4.8.1/nuget.exe"
    $parent = [System.IO.Path]::GetTempPath() + "DevProxy"
    $targetNugetExe = "$parent\nuget.exe"
    $temp = New-Item -Path $parent -ItemType Directory -Force
    Invoke-WebRequest $sourceNugetExe -OutFile $targetNugetExe
    
    return $parent
}

function Remove-GitProxy ([String]$path) {
    Write-Host "Using git from $path"
    Write-Host "Removing existing proxy..."
    $temp = Start-Process -FilePath $path -ArgumentList "config", "--global", "--unset", "http.proxy" -PassThru -Wait -NoNewWindow

    Write-Host "Done"
}

function Remove-NugetProxy ([String]$path) {
    Write-Host "Using nuget from $path\nuget.exe"
    Write-Host "Removing existing proxy..."
    $temp = Start-Process -FilePath "$path\nuget.exe" -ArgumentList "config", "-set", "http_proxy="  -PassThru -Wait

    Write-Host "Done"
}

function Test-NodeExists {
    $nodePath = "C:\Program Files\nodejs\node.exe"
    $nodeModulePath = "C:\Program Files\nodejs\node_modules\npm\bin\npm-cli.js"

    if ((Test-Path -Path $nodePath) -And (Test-Path -Path $nodeModulePath)) {
        return $true
    }

    return $false
}

function Remove-NpmProxy ([String]$path) {
    Write-Host "########################################"
    Write-Host "# # # This will take a few moments # # #"
    Write-Host "########################################"
    Write-Host
    Write-Host "Using npm from $path"
    Write-Host "Removing existing proxy..."
    $temp = Start-Process -FilePath $path -ArgumentList "config", "delete", "proxy" -PassThru -Wait -NoNewWindow
    Write-Host "Done"
}

Clear-Host

Write-Host "*** Updating Git Proxy ***"
$gitPath = Find-Git
Remove-GitProxy($gitPath)
Write-Host "*** Git Proxy Update Complete ***\"

Write-Host


Write-Host "*** Updating Nuget Proxy ***"
$nugetDirectory = Get-NugetFromWeb
Remove-NugetProxy($nugetDirectory)
Remove-Item -Path $nugetDirectory -Force -Recurse
Write-Host "*** Nuget Proxy Update Complete ***"

Write-Host

if (Test-NodeExists) {
    Write-Host "*** Updating NPM Proxy ***"
    $nodePath = Find-Npm
    Remove-NpmProxy($nodePath)
    Write-Host "*** NPM Proxy Update Complete ***"
}
