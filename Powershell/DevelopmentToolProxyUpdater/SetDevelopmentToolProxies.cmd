@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET me=%~n0
SET parent=%~dp0

SET /A SUCCESS=0

SET powerShell="%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe"
%powerShell% -NoProfile -ExecutionPolicy Bypass -File "SetDevToolProxies.ps1"
IF %ErrorLevel% NEQ 0 (
    EXIT /B %ErrorLevel%
)
EXIT /B %SUCCESS%
