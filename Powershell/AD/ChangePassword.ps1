$AccountName = "brad.knowles"
Set-AdAccountPassword -Identity $AccountName -OldPassword (Read-Host -asSecureString "Enter the current password") -NewPassword (Read-Host -asSecureString "Enter the new password")
