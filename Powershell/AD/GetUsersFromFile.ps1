<# 
    users.txt can contain any of these attributes

    A distinguished name
    A GUID (objectGUID)
    A security identifier (objectSid)
    A SAM account name (sAMAccountName)
#>

$userNamesList = Get-Content -Path "users.txt"
$exportPath = "UserAdInfo.csv"

foreach ($name in $userNamesList) {
    Get-ADUser $name -Properties * `
     | Select-Object Name, Title, Department, Enabled `
     | Export-CSV $ExportPath -Append -NoTypeInformation
}
