function Handle-Error {
  Pop-Location
  exit $LastExitCode
}

Push-Location -Path "$PSScriptRoot"

Clear-Host
Write-Host "rbd" -ForegroundColor Yellow
rbd

Write-Host
Write-Host "dotnet restore" -ForegroundColor Yellow
dotnet restore
If ($LastExitCode -ne 0) {
    Handle-Error
  }

Write-Host
Write-Host "dotnet build --configuration Release --no-restore -warnaserror" -ForegroundColor Yellow
dotnet build --configuration Release --no-restore -warnaserror
If ($LastExitCode -ne 0) {
  Handle-Error
  }

Write-Host
Write-Host "dotnet format --severity warn --verify-no-changes --no-restore --verbosity diagnostic" -ForegroundColor Yellow
dotnet format --severity warn --verify-no-changes --no-restore --verbosity diagnostic
If ($LastExitCode -ne 0) {
  Handle-Error
  }

Write-Host
Write-Host "dotnet build --configuration Release --no-restore -warnaserror" -ForegroundColor Yellow
dotnet build --configuration Release --no-restore -warnaserror
If ($LastExitCode -ne 0) {
  Handle-Error
  }

Write-Host
Write-Host "dotnet test --no-restore --logger 'console;verbosity=normal'" -ForegroundColor Yellow
dotnet test --no-restore --logger 'console;verbosity=normal'
If ($LastExitCode -ne 0) {
  Handle-Error
  }

Pop-Location
