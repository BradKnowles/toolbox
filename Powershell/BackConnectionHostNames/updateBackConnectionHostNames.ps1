﻿$NewValue = "pex-vm-ngx-04_vip1.quantum.qh-quantum.com"

$RegistyPath = "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa\MSV1_0"
$RegistyKey = "BackConnectionHostNames"

$BackConnectionHostNames = (Get-ItemProperty $RegistyPath $RegistyKey).$RegistyKey
$BackConnectionHostNames = $BackConnectionHostNames + $NewValue

Set-ItemProperty -Path $RegistyPath -Name $RegistyKey -Type MultiString -Value $BackConnectionHostNames
