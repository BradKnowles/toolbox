﻿$siteName = "bqerenderingservice.quantum.qh-quantum.com"
$protocol = "https"


$subdomainName = "bqerenderingservice"
$directSubdomainName = "bqerenderingservice1"
$domain = ".quantum.qh-quantum.com"
$ipAddress = "10.255.144.61"
$port = 443

$sslThumbprint = "‎54 c8 31 4f a3 17 5d 45 2c 01 90 52 da e7 d8 82 05 76 37 2b"

New-WebBinding -Name $siteName -Protocol $protocol -HostHeader $subdomainName -IPAddress $ipAddress -Port $port
(Get-WebBinding -Name $siteName -Protocol $protocol -HostHeader $subdomainName -IPAddress $ipAddress -Port $port).AddSslCertificate($sslThumbprint)

New-WebBinding -Name $siteName -Protocol $protocol -HostHeader $subdomainName + $domain -IPAddress $ipAddress -Port $port
(Get-WebBinding -Name $siteName -Protocol $protocol -HostHeader $subdomainName + $domain -IPAddress $ipAddress -Port $port).AddSslCertificate($sslThumbprint)

New-WebBinding -Name $siteName -Protocol $protocol -HostHeader $directSubdomainName + $domain -IPAddress $ipAddress -Port $port
(Get-WebBinding -Name $siteName -Protocol $protocol -HostHeader $directSubdomainName + $domain -IPAddress $ipAddress -Port $port).AddSslCertificate($sslThumbprint)
