﻿Import-Module WebAdministration
$appPoolName = ""

$propertyName = "startMode"
$propertyValue = "AlwaysRunning"

Get-ItemProperty "IIS:\AppPools\$appPoolName"  -Name $propertyName
Set-ItemProperty "IIS:\AppPools\$appPoolName" -Name $propertyName -Value $propertyValue
