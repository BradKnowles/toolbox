$appName = "benefitcheckerworkerdev1.quantum.qh-quantum.dev"
$iisLocation = "C:\inetpub\" + $appName

#$appPoolCreds = Get-Credential

New-WebAppPool -Name $appName -Force
Set-ItemProperty IIS:\AppPools\$appName -Name startMode -Value AlwaysRunning

Set-ItemProperty IIS:\AppPools\$appName -Name processModel.identityType -Value SpecificUser 
Set-ItemProperty IIS:\AppPools\$appName -Name processModel.userName -Value $appPoolCreds.UserName

$netCred = New-Object System.Net.NetworkCredential "unused", $appPoolCreds.password
Set-ItemProperty IIS:\AppPools\$appName -Name processModel.password -Value $netCred.Password
Set-ItemProperty IIS:\AppPools\$appName -Name processModel.loadUserProfile -Value False
Set-ItemProperty IIS:\AppPools\$appName -Name recycling.periodicRestart.time -Value ([TimeSpan]::FromDays(29))

#mkdir $iisLocation
New-Website -Name $appName -PhysicalPath $iisLocation -ApplicationPool $appName -Ssl -HostHeader $appName -Port 443 
