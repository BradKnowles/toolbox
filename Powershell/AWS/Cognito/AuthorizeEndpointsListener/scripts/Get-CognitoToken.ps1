param (
    [switch]$Reset
)

. "$PSScriptRoot\Functions.ps1"

$cognitoProperties = @{}
$cognitoConfigPath = "$PSScriptRoot\..\.config\cognito-config.json"

if ($Reset) {
    Remove-Item -Path $cognitoConfigPath -ErrorAction SilentlyContinue
}

if (Test-Path -Path $cognitoConfigPath -PathType Leaf) {
    $cognitoProperties = Get-Content -Path $cognitoConfigPath -Raw | ConvertFrom-Json
} else {
    Get-UserPool -CognitoProperties $cognitoProperties
    Get-ClientId -CognitoProperties $cognitoProperties
    Get-Domain -CognitoProperties $cognitoProperties
    Get-RedirectUri -CognitoProperties $cognitoProperties
    $cognitoProperties | ConvertTo-Json | Set-Content -Path $cognitoConfigPath
}

Invoke-LoginPage $cognitoProperties
$httpListener = Start-RedirectUriListener $cognitoProperties
$authorizationCode = Get-LoginResponse $httpListener
Stop-RedirectUriListener $httpListener
$accessToken = Get-AccessToken $authorizationCode $cognitoProperties

Set-Clipboard -Value $accessToken
Write-Host The following access token has been copied to the clipboard -ForegroundColor Green
Write-Host $accessToken -ForegroundColor DarkGray
