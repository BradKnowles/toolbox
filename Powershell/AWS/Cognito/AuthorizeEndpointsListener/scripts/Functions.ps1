. "$PSScriptRoot\Write-Menu.ps1"

function ConvertTo-Entries {
    param (
        $hash
    )

    $result = @{}
    switch ($hash.GetType().Name) {
        'PSCustomObject' {
            $result[$hash.Name + ':'+ $hash.Id] = 'Write-Output "' + $hash.Id + '"'
        }
        'Object[]' {
            $hash.GetEnumerator() | ForEach-Object {
                $result[$_.Name + ':'+ $_.Id] = 'Write-Output "' + $_.Id + '"'
            }
        }
    }
    return $result
}

function Get-AuthCode {
    param (
        [String]$PrefixUri
    )

    $httpListener = New-Object System.Net.HttpListener
    $httpListener.Prefixes.Add($PrefixUri)
    $httpListener.Start()

    $context = $httpListener.GetContext()

    $authCode = $context.Request.QueryString["code"]

    $context.Response.StatusCode = 200
    $context.Response.ContentType = "text/html"
    $response = Get-Content -Path "$PSScriptRoot\.config\cognito-response.html"
    $responseBytes = [System.Text.Encoding]::UTF8.GetBytes($response)
    $context.Response.OutputStream.Write($responseBytes, 0, $responseBytes.Length)
    $context.Response.Close()
    $httpListener.Close()


    return $authCode
}

function Get-AccessToken {
    param (
        $AuthCode,
        $Properties
    )

    $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $headers.Add("Content-Type", "application/x-www-form-urlencoded")
    $body = "grant_type=authorization_code&client_id={0}&code=$AuthCode&redirect_uri={1}" -f $Properties.ClientId, $Properties.RedirectUri

    $url = "{0}/oauth2/token/" -f $Properties.UserPoolDomain
    $response = Invoke-RestMethod $url  -Method 'POST' -Headers $headers -Body $body
    Set-Clipboard -Value $response.access_token
    Write-Host The following access token has been copied to the clipboard -ForegroundColor Green
    Write-Host $response.access_token -ForegroundColor DarkGray

}

function Start-LoginProcess {
    param (
        $Properties
    )

    $url = "{0}/oauth2/authorize?client_id={1}&redirect_uri={2}&response_type=code" -f $Properties.UserPoolDomain, $Properties.ClientId, $Properties.RedirectUri
    Start-Process $url
}

function Get-UserPool {
    param (
        $CognitoProperties
    )

    $userPools = aws cognito-idp list-user-pools --max-results 10 --query 'UserPools[].{Name: Name, Id: Id}' --output json | ConvertFrom-Json
    $entries = ConvertTo-Entries -hash $userPools
    $userPoolId = Write-Menu -Title "Available Cognito User Pools" -Sort -Entries $entries
    $CognitoProperties["UserPoolId"] = $userPoolId
}

function Get-ClientId {
    param (
        $CognitoProperties
    )

    $userPoolClients = aws cognito-idp list-user-pool-clients --user-pool-id $CognitoProperties.UserPoolId --query 'UserPoolClients[].{Name: ClientName, Id: ClientId}' --output json | ConvertFrom-Json
    $entries = ConvertTo-Entries -hash $userPoolClients
    $userPoolClientId = Write-Menu -Title "Available User Pool Clients in $($CognitoProperties.UserPoolId)" -Sort -Entries $entries
    $CognitoProperties["ClientId"] = $userPoolClientId
}

function Get-Domain {
    param (
        $CognitoProperties
    )

    $domain = aws cognito-idp describe-user-pool --user-pool-id $CognitoProperties.UserPoolId --query 'UserPool.{Arn: Arn, Domain: Domain, CustomDomain: CustomDomain}' --output json | ConvertFrom-Json
    $arn = $domain.Arn -split ":"
    $CognitoProperties["UserPoolDomain"] = "https://$($domain.Domain).auth.$($arn[3]).amazoncognito.com"
}

function Get-RedirectUri {
    param (
        $CognitoProperties
    )

    $redirectUri = aws cognito-idp describe-user-pool-client --user-pool-id $CognitoProperties.UserPoolId --client-id $CognitoProperties.ClientId --query "UserPoolClient.CallbackURLs[?contains(@, 'localhost')] | [0]" --output text
    $CognitoProperties["RedirectUri"] = $redirectUri
}

function Invoke-LoginPage {
    param (
        $CognitoProperties
    )

    $url = "{0}/oauth2/authorize?client_id={1}&redirect_uri={2}&response_type=code" -f $CognitoProperties.UserPoolDomain, $CognitoProperties.ClientId, $CognitoProperties.RedirectUri
    Start-Process $url
}

function Start-RedirectUriListener {
    param (
        $CognitoProperties
    )

    $httpListener = New-Object System.Net.HttpListener
    $httpListener.Prefixes.Add($CognitoProperties.RedirectUri)
    $httpListener.Start()
    return $httpListener
}

function Get-LoginResponse {
    param (
        $HttpListener
    )

    $context = $HttpListener.GetContext()

    $authorizationCode = $context.Request.QueryString["code"]

    $context.Response.StatusCode = 200
    $context.Response.ContentType = "text/html"

    $responseContent = Get-Content -Path "$PSScriptRoot\..\.config\cognito-response.html"
    $responseBytes = [System.Text.Encoding]::UTF8.GetBytes($responseContent)
    $context.Response.OutputStream.Write($responseBytes, 0, $responseBytes.Length)

    $context.Response.Close()

    return $authorizationCode
}

function Stop-RedirectUriListener {
    param (
        $HttpListener
    )

    $HttpListener.Close()
}

function Get-AccessToken {
    param (
        $AuthorizationCode,
        $CognitoProperties
    )

    $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $headers.Add("Content-Type", "application/x-www-form-urlencoded")
    $body = "grant_type=authorization_code&client_id={0}&code=$AuthorizationCode&redirect_uri={1}" -f $CognitoProperties.ClientId, $CognitoProperties.RedirectUri

    $url = "{0}/oauth2/token/" -f $CognitoProperties.UserPoolDomain
    $response = Invoke-RestMethod $url -Method 'POST' -Headers $headers -Body $body

    return $response.access_token
}
