﻿Param(
[Parameter(Mandatory=$true)]
[string]$Cname
)

$dc = "phq-dc-06"

$dnszone= Get-DnsServerZone -ComputerName $dc  | Where-Object {$_.IsReverseLookupZone -like "False"}

$dnszone | ForEach-Object $_ {
    $zonename = $_.ZoneName    
    $ZoneRecordData = Get-DnsServerResourceRecord -ZoneName $zonename -Type 5 -ComputerName $dc  

    $data = $ZoneRecordData | Where-Object {$_.HostName -like "*$Cname*"}
    if ($data -ne $null) {
        if ($data.GetType().IsArray) {
            $data.ForEach({

                $result = [PSCustomObject]@{
                    HostName = $_.HostName
                    Zone = $zonename
                    HostNameAlias = $_.RecordData.HostNameAlias
                }

                Write-Output ($result)
            })
        }
        Else {
            $result = [PSCustomObject]@{
                HostName = $data.HostName
                Zone = $zonename
                HostNameAlias = $data.RecordData.HostNameAlias
            }

            Write-Output ($result)
        }
    }
}
