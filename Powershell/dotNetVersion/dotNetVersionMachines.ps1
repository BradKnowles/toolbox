if ($null -eq $adminCredientials) {
    $adminCredientials = Get-Credential
}

Invoke-Command -ComputerName (Get-Content .\machines.txt) -FilePath .\dotNetVersion.ps1 -Credential $adminCredientials